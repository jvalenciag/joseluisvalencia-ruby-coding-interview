class Tweet < ApplicationRecord
  belongs_to :user
  validates :body, length: {maximum: 180}

  validate :validate_body

  def validate_body
    if Tweet.where(body: body, user_id: user_id, created_at: [Time.now - 1.day.. Time.now]).any?
      errors.add(:body, "Duplicated Tweet")
    end
  end
end
